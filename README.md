# Docker Example With WSL2

1. [Requirements](#requirements)
    1. [Intended OS and Dev Env](#os)
    2. [WSL2 Installation](#wsl-i)
    3. [Docker Installation](#docker-i)
2. [How To Run](#run)
   1. [Local](#local-r)
   2. [Docker](#docker-r)

## Requirements <a name="requirements"></a> 

### Intended OS and Dev Env <a name="os"></a>

Intended to be used on Windows 10 with WSL2 with Ubuntu 20.04 LTS. The Golang version used is 1.13.8, and Docker 20.10.8.

### WSL2 Installation <a name="wsl2-i"></a>

Open CMD in Windows as administrator and run the following command:

```wsl --install```

Restart computer and open Ubuntu. 

### Docker installation <a name="docker-i"></a>

https://docs.docker.com/desktop/windows/install/

Follow the **WSL Backend** guide in the link above. 

In the WSL2 terminal do the following commands: 

```touch /mnt/c/Users/<Windows User Name>/.wslconfig```

```nano /mnt/c/Users/<Windows User Name>/.wslconfig``` 

Paste: 

```
[wsl2]
kernel=C:\\temp\\myCustomKernel
memory=4GB # Limits VM memory in WSL 2 to 4 GB
processors=2 # Makes the WSL 2 VM use two virtual processors
```

And save by doing: 

```[CTRL] + O``` -> ```[ENTER]``` -> ```[CTRL] + X``` -> ```[ENTER]```

Run the following commands in sequence:

```
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Restart WSL2 and test the installation by doing the following command in the WSL2 Ubuntu window:

```sudo docker run hello-world```

You should see the image appear in the Docker Dashboard on Windows and that there was a successful run of the 
container.


## How to run <a name="run"></a>

Clone the project and step in to the folder with:

```cd docker-example```

### Local <a name="local-r"></a>

Build the docker image with:

```sudo go build -o ./binary```

Run by executing ```./binary```

Now we can do the following to test the application in a new window:

```curl http://localhost:8080/Name_Here```

### Docker <a name="docker-r"></a>
 
Build the docker image with:

```docker build --tag docker-example .```

Check if image exists once build is done: 

```docker imagaes``` which should show:

```
REPOSITORY       TAG       IMAGE ID       CREATED          SIZE
docker-example   latest    0433e0e7ff73   18 seconds ago   368MB
hello-world      latest    d1165f221234   5 months ago     13.3kB
```

Now we want to run this image as a container, which we do by: 

```docker run --detach --publish 8080:8080 docker-example```

We can check that the container is running by:

```docker ps``` where we would see: 

```
CONTAINER ID   IMAGE            COMMAND             CREATED              STATUS              PORTS                                       NAMES
2624b5554925   docker-example   "/docker-example"   About a minute ago   Up About a minute   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   mystifying_turing
```

Now we can do the following to test the application: 

```curl http://localhost:8080/Name_Here```

**Important** Remember to stop container when you are done: 

```docker stop eloquent_goldberg```

Confirm with doing ```docker ps``` again. Note that the containers are not completely gone,
just stopped. To see this do ```docker ps -a```, my return on that command is:

```
CONTAINER ID   IMAGE            COMMAND             CREATED          STATUS                      PORTS     NAMES
d7a35632e1d1   docker-example   "/docker-example"   3 minutes ago    Exited (2) 35 seconds ago             eloquent_goldberg
2624b5554925   docker-example   "/docker-example"   14 minutes ago   Exited (2) 3 minutes ago              mystifying_turing
1a0d01ffd101   docker-example   "/docker-example"   17 minutes ago   Exited (2) 14 minutes ago             lucid_leavitt
```

To completely delete them in order to keep it clean, do: 

```docker rm eloquent_goldberg mystifying_turing lucid_leavitt``` 

And confirm with ```docker ps -a```